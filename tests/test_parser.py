"""Module to test parser.py"""

from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_oct_converter.parser import parse_config

dummy_config = {
    "debug": False,
    "output_image_type": ".png",
    "output_video_type": ".mp4",
    "output_dicom": True,
    "output_npy": True,
    "montage_columns": 0,
    "montage_rows": 0,
    "e2e_fundus_spacing": "0.005 0.005",
    "img_rows": 1024,
    "img_columns": 512,
    "img_interlaced": False,
}


@pytest.mark.parametrize(
    "filename, filetype",
    [
        ("test.fda", "topcon_fda"),
        ("test.fds", "topcon_fds"),
        ("test.e2e", "heidelberg_e2e"),
        ("test.img", "zeiss_img"),
        ("test.OCT", "bioptigen_oct"),
    ],
)
def test_parse_config(tmp_path, filename, filetype):
    """Test for successful parsing of most filetypes (all except POCT)"""
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    (tmp_path / "work").mkdir()
    gear_context.work_dir = tmp_path / "work"
    gear_context.output_dir = tmp_path / "output"
    fake_input = tmp_path / filename
    fake_input.touch()

    def _get_input(name):
        nonlocal fake_input
        if name == "raw_input":
            return fake_input
        return None

    gear_context.get_input_path.side_effect = _get_input

    debug, config_dict = parse_config(gear_context)

    assert not debug
    assert config_dict["output_image_type"] == dummy_config["output_image_type"]
    assert config_dict["output_video_type"] == dummy_config["output_video_type"]
    assert config_dict["output_dicom"] == dummy_config["output_dicom"]
    assert config_dict["montage_columns"] == dummy_config["montage_columns"]
    assert config_dict["montage_rows"] == dummy_config["montage_rows"]
    assert config_dict["img_columns"] == dummy_config["img_columns"]
    assert config_dict["img_rows"] == dummy_config["img_rows"]
    assert config_dict["img_interlaced"] == dummy_config["img_interlaced"]
    assert config_dict["output_dir"] == tmp_path / "output"
    assert config_dict["work_path"] == tmp_path / f"work/{filename}"
    assert config_dict["filetype"] == filetype


def test_parse_config_poct(tmp_path):
    """Test for successful parsing of POCT filetype"""
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    (tmp_path / "work").mkdir()
    gear_context.work_dir = tmp_path / "work"
    gear_context.output_dir = tmp_path / "output"
    fake_input = tmp_path / "test.OCT"
    fake_input.touch()
    fake_txt = tmp_path / "test.txt"
    fake_txt.touch()

    def _get_input(name):
        nonlocal fake_input
        nonlocal fake_txt
        if name == "raw_input":
            return fake_input
        elif name == "poct_txt":
            return fake_txt
        return None

    gear_context.get_input_path.side_effect = _get_input

    debug, config_dict = parse_config(gear_context)

    assert not debug
    assert config_dict["output_image_type"] == dummy_config["output_image_type"]
    assert config_dict["output_video_type"] == dummy_config["output_video_type"]
    assert config_dict["output_dicom"] == dummy_config["output_dicom"]
    assert config_dict["montage_columns"] == dummy_config["montage_columns"]
    assert config_dict["montage_rows"] == dummy_config["montage_rows"]
    assert config_dict["img_columns"] == dummy_config["img_columns"]
    assert config_dict["img_rows"] == dummy_config["img_rows"]
    assert config_dict["img_interlaced"] == dummy_config["img_interlaced"]
    assert config_dict["output_dir"] == tmp_path / "output"
    assert config_dict["work_path"] == tmp_path / "work/test.OCT"
    assert config_dict["filetype"] == "optovue_oct"


def test_parse_config_fail_unsupported_filetype(tmp_path, caplog):
    """Test for gear failing on unsupported filetype."""
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    (tmp_path / "work").mkdir()
    gear_context.work_dir = tmp_path / "work"
    gear_context.output_dir = tmp_path / "output"
    fake_input = tmp_path / "wrong.csv"
    fake_input.touch()

    def _get_input(name):
        nonlocal fake_input
        if name == "raw_input":
            return fake_input
        return None

    gear_context.get_input_path.side_effect = _get_input

    with pytest.raises(SystemExit):
        parse_config(gear_context)

    assert "not currently supported" in caplog.text


def test_parse_config_fail_no_output_selected(tmp_path, caplog):
    """Test for gear failing on no outputs selected."""
    no_output_config = {
        "debug": False,
        "output_image_type": "none",
        "output_video_type": "none",
        "output_dicom": False,
        "output_npy": False,
        "montage_columns": 0,
        "montage_rows": 0,
        "img_rows": 1024,
        "img_columns": 512,
        "img_interlaced": False,
    }
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = no_output_config
    (tmp_path / "work").mkdir()
    gear_context.work_dir = tmp_path / "work"
    gear_context.output_dir = tmp_path / "output"
    fake_input = tmp_path / "test.fda"
    fake_input.touch()

    def _get_input(name):
        nonlocal fake_input
        if name == "raw_input":
            return fake_input
        return None

    gear_context.get_input_path.side_effect = _get_input

    with pytest.raises(SystemExit):
        parse_config(gear_context)

    assert "No outputs selected." in caplog.text
