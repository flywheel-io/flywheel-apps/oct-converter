"""Module to test main.py"""

import pytest

from fw_gear_oct_converter.main import run


@pytest.fixture
def mock_factory(mocker):
    mock_factory = mocker.patch(
        "fw_gear_oct_converter.converter.OphthaConverter.factory"
    )
    return mock_factory


def test_run(tmp_path, mock_factory):
    config_dict = {
        "output_image_type": ".png",
        "output_video_type": ".mp4",
        "output_dicom": True,
        "montage_rows": 5,
        "montage_columns": 5,
        "img_rows": 1024,
        "img_columns": 512,
        "img_interlaced": False,
        "work_path": tmp_path / "fake_file.img",
        "output_dir": tmp_path,
        "filetype": "zeiss_img",
    }

    exit_code = run(config_dict=config_dict)

    assert exit_code == 0
    mock_factory.assert_called_once()
