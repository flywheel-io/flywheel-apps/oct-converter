import os

import pytest

from fw_gear_oct_converter.converter import (
    BioptigenOCTOphthaConverter,
    HeidelbergOphthaConverter,
    OphthaConverter,
    OptovueOCTOphthaConverter,
    TopconFDAOphthaConverter,
    TopconFDSOphthaConverter,
    ZeissOphthaConverter,
)

dummy_config = config_dict = {
    "output_image_type": ".png",
    "output_video_type": ".mp4",
    "output_dicom": True,
    "output_npy": True,
    "montage_columns": 0,
    "montage_rows": 0,
    "img_rows": 1024,
    "img_columns": 512,
    "img_interlaced": True,
    "e2e_scalex": 0.01,
    "e2e_slice_thickness": 0.05,
    "e2e_extract_scan_repeats": False,
}


@pytest.mark.parametrize(
    "filetype, ext, converter",
    [
        ("zeiss_img", "img", ZeissOphthaConverter),
        ("topcon_fds", "fds", TopconFDSOphthaConverter),
        ("topcon_fda", "fda", TopconFDAOphthaConverter),
        ("heidelberg_e2e", "e2e", HeidelbergOphthaConverter),
        ("bioptigen_oct", "OCT", BioptigenOCTOphthaConverter),
        ("optovue_oct", "OCT", OptovueOCTOphthaConverter),
    ],
)
def test_factory_file_not_found(filetype, ext, converter, tmp_path):
    """
    Test factory for different file types with nonexistant files
    """
    dummy_config["output_dir"] = tmp_path
    dummy_config["work_path"] = tmp_path / f"{filetype}.{ext}"
    dummy_config["filetype"] = filetype

    with pytest.raises(FileNotFoundError):
        ophtha_converter = OphthaConverter.factory(config_dict)
        assert isinstance(ophtha_converter, converter)


def test_factory_read_img(tmp_path):
    """
    Check instantiation of OphthaConverter with img file
    """
    dummy_config["output_dir"] = tmp_path
    dummy_config["work_path"] = os.path.abspath("tests/assets/cube_raw.img")
    dummy_config["filetype"] = "zeiss_img"

    ophtha_converter = OphthaConverter.factory(dummy_config)

    assert isinstance(ophtha_converter, ZeissOphthaConverter)
    assert ophtha_converter.file_ext == ".img"
    assert ophtha_converter.oct_volumes == []
    assert ophtha_converter.fundus_images == []
    assert ophtha_converter.dicoms == []

    ophtha_converter.read_file()

    assert len(ophtha_converter.oct_volumes) == 1
    assert ophtha_converter.fundus_images == []
    assert ophtha_converter.dicoms == []

    ophtha_converter._save_output()

    assert (tmp_path / "cube_raw.png").exists()
    assert (tmp_path / "cube_raw.mp4").exists()
    assert (tmp_path / "cube_raw.npy").exists()
