#!/usr/bin/env python
"""The run script"""

import logging
import os
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_oct_converter.main import run
from fw_gear_oct_converter.parser import parse_config

log = logging.getLogger(__name__)


def main(gear_context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""
    debug, config_dict = parse_config(gear_context=gear_context)
    e_code = run(config_dict)

    # Tag input file
    if input_tag := gear_context.config.get("input_tag"):
        gear_context.metadata.add_file_tags(
            file_=context.get_input("raw_input"), tags=input_tag
        )

    # Tag output files
    if output_tag := gear_context.config.get("output_tag"):
        for file in os.listdir(str(gear_context.output_dir)):
            gear_context.metadata.add_file_tags(file, tags=output_tag)

    sys.exit(e_code)


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as context:
        context.init_logging()
        main(context)
