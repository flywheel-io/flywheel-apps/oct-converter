# oct-converter: OCT Converter

## Overview

This gear converts raw OCT formats from manufacturers (e.g., .E2E, .OCT) to
publicly viewable/accessible formats. Output types include numpy (.npy), video formats
(.mp4, .avi), image formats (.png, .tiff, .jpg, .jpeg), and DICOM. OCT image output
is in mosaic format, which is a plot of several image slices.

### Summary

A Flywheel Gear for extracting the raw optical coherence tomography (OCT) and fundus
data from manufacturer's proprietary file formats. This Gear uses the oct-converter
package to read in raw image data in a variety of formats (.fds (Topcon), .fda
(Topcon), .e2e (Heidelberg), .img (Zeiss), .OCT (Optovue), .OCT (Bioptigen)) and write
out those data into more open formats. This Gear will output a NumPy array
(containing the raw data in .npy format), and optional image, video, and/or DICOM.

### Cite

This oct-converter gear utilizes OCT-Converter, (C) 2021 Mark Graham
(<https://github.com/marksgraham/OCT-Converter>).

### License

*License:* MIT

### Classification

*Category:* converter

*Gear Level:*

- [ ] Project
- [ ] Subject
- [ ] Session
- [X] Acquisition
- [ ] Analysis

----

[[*TOC*]]

----

### Inputs

- *raw_input*
  - __Name__: *raw_input*
  - __Type__: *file*
  - __Optional__: *False*
  - __Description__: *Raw OCT or Fundus data. Supported formats are: .fds (Topcon);
  .fda (Topcon); .e2e (Heidelberg); .img (Zeiss); .OCT (Bioptigen or Optovue)*
- *poct_txt*
  - __Name__: *poct_txt*
  - __Type__: *file*
  - __Optional__: *True*
  - __Description__: *For Optovue OCT conversion, the converter requires a txt file
  with the same name as the .OCT file.*
  - __Notes__: *If inputted, converter will parse the filetype as Optovue; do not
  use this input for any other filetype.*

### Config

- *debug*
  - __Name__: *debug*
  - __Type__: *boolean*
  - __Description__: *Log debug messages*
  - __Default__: *False*
- *output_dicom*
  - __Name__: *output_dicom*
  - __Type__: *boolean*
  - __Description__: *Whether to convert input file to DICOM (supported filetypes:
  .e2e, .fda, .fds, .img, .OCT).*
  - __Default__: *False*
- *output_image_type*
  - __Name__: *output_image_type*
  - __Type__: *string*
  - __Description__: *The type of image to output. Type must be either of the
  following: ['.png', '.bmp', '.tiff', '.jpg', '.jpeg'], or 'none' for no image.*
  - __Default__: *".png"*
- *output_video_type*
  - __Name__: *output_video_type*
  - __Type__: *string*
  - __Description__: *The type of video to output. Type must be either of the
  following: ['.avi', '.mp4'], or 'none' for no video.*
  - __Default__: *".mp4"*
- *output_npy*
  - __Name__: *output_npy*
  - __Type__: *boolean*
  - __Description__: *Whether to convert input file to .npy output.*
  - __Default__: *True*
- *montage_rows*
  - __Name__: *montage_rows*
  - __Type__: *integer*
  - __Description__: *Optional - used with peek(). The number of rows in resulting
  montage image*
  - __Optional__: *True*
- *montage_columns*
  - __Name__: *montage_columns*
  - __Type__: *integer*
  - __Description__: *Optional - used with peek(). The number of columns in resulting
  montage image*
  - __Optional__: *True*
- *e2e_scalex*
  - __Name__: *e2e_scalex*
  - __Type__: *float*
  - __Description__: *Optional - used with Heidelberg .e2e files for manual control
  of setting x-axis PixelSpacing. If not set, defaults to 0.01.*
  - __Optional__: *True*
- *e2e_slice_thickness*
  - __Name__: *e2e_slice_thickness*
  - __Type__: *float*
  - __Description__: *Optional - used with Heidelberg .e2e OCT Volume files for manual
  control of setting SliceThickness. If not set, defaults to 0.05.*
  - __Optional__: *True*
- *e2e_extract_scan_repeats*
  - __Name__: *e2e_extract_scan_repeats*
  - __Type__: *boolean*
  - __Description__: *Used with Heidelberg .e2e files with fundus images to extract all
  fundus images, including those that appear repeated.*
  - __Default__: *False*
- *img_columns*
  - __Name__: *img_columns*
  - __Type__: *integer*
  - __Description__: *Optional - used with Zeiss .img files for manual control of
  setting columns.*
  - __Optional__: *True*
- *img_rows*
  - __Name__: *img_rows*
  - __Type__: *integer*
  - __Description__: *Optional - used with Zeiss .img files for manual control of
  setting rows.*
  - __Optional__: *True*
- *img_interlaced*
  - __Name__: *img_interlaced*
  - __Type__: *boolean*
  - __Description__: *Optional - used with Zeiss .img files for manual control of
  setting interlaced.*
  - __Optional__: *True*
- *input_tag*
  - __Name__: *input_tag*
  - __Type__: *string*
  - __Description__: *Optional - if provided, tags raw_input file with the given tag.*
  - __Default__: *"oct-converter"*
- *output_tag*
  - __Name__: *output_tag*
  - __Type__: *string*
  - __Description__: *Optional - if provided, tags output files with the given tag.*
  - __Default__: *"oct-converter"*

### Outputs

#### Files

- *Output Image*
  - __Name__: *Output Image*
  - __Type__: *.png, .tiff, .jpg, or .jpeg as configured*
  - __Optional__: *True*
  - __Description__: *Resulting OCT images as a montage, and fundus image if
  applicable*
- *Output Video*
  - __Name__: *Output Video*
  - __Type__: *.mp4 or .avi as configured*
  - __Optional__: *True*
  - __Description__: *Resulting OCT images as a video*
- *Output DICOM*
  - __Name__: *Output DICOM*
  - __Type__: *.dcm*
  - __Optional__: *True*
  - __Description__: *Resulting DICOM from OCT images and fundus if applicable*
  - __Notes__: *DICOM tags are populated from the metadata able to be gleaned from the
  input. Some filetypes have considerably more complete metadata than others.*
- *NumPy Array*
  - __Name__: *NumPy Array*
  - __Type__: *.npy*
  - __Optional__: *False*
  - __Description__: *NumPy Array of raw data*

#### Metadata

No Flywheel metadata are created by this gear, however if `output_dicom = True`,
available metadata will be populated in the appropriate DICOM tags.

### Pre-requisites

No prerequisites are identified to run this gear.

#### Prerequisite Gear Runs

No prerequisite gear runs are identified as needed before running this gear.

#### Prerequisite Metadata

No prerequisite Flywheel metadata are needed to run this gear.

## Usage

### Description

This gear utilizes the [OCT-Converter](https://github.com/marksgraham/OCT-Converter)
library to facilitate the reading of a variety of OCT filetypes from different
manufacturers. OCT-Converter provides the ability to parse bytestrings into
python-readable arrays and metadata, which can then be converted into a variety of
different filetypes for use in Flywheel.

#### File Specifications

##### *raw_input*

The primary input for this gear is the imaging file created by an ophthalmic scanner.
This file may be .e2e (Heidelberg), .img (Zeiss), .fda/.fds (Topcon), .OCT (Bioptigen),
or .OCT (Optovue). Each filetype has a different binary structure with one or more OCT
or fundus images. Some filetypes contain additional metadata, other filetypes are known
to utilize file naming conventions to convey a small collection of details (i.e.
acquisition date, laterality).

##### *poct_txt*

Specific to Optovue is a secondary input txt file that contains information needed for
the converter. This file is required to process Optovue .OCT files; this input should
not be utilized for anything other than this txt file. OCT-Converter expects this file
to be named the same as its corresponding .OCT file (i.e "poct.OCT" would be paired
with "poct.txt").

### OCT to DICOM Metadata

Proprietary filetypes have varying metadata available to populate the DICOM header.
Some known limitations should be taken into account when utilizing this gear.

- .e2e (Heidelberg)
  - Metadata is partially extracted, with many fields unknown
    - Pixel spacing is obfuscated, with only "scale y" available from the file
    - Values for "scale x" and "slice thickness" are set to defaults that allow for
    viewing the image within the Flywheel viewer
    - Config options `e2e_scalex` and `e2e_slice_thickness` can be set to known values
    by the user
- .fda and .fds (Topcon)
  - Metadata is available, including scale for viewing and measurements
- .img (Zeiss)
  - File naming may have patient ID, acquisition date, and laterality but not
  guaranteed
  - Pixel spacing is not available and values are set to defaults that allow for
  viewing the image within the Flywheel viewer
- .oct (Bioptigen)
  - Minimal metadata available (scan type, acquisition datetime)
  - File naming may have laterality and patient_id in filename but not guaranteed
  - Pixel spacing is not available and values are set to defaults that allow for
  viewing the image within the Flywheel viewer
- .oct (Optovue)
  - Requires a paired .txt file for metadata
  - Very basic metadata, including laterality and dimensions
  - File naming may have acquisition date in filename but not guaranteed

### Workflow

```mermaid
graph LR;
    A[raw_input]:::input --> E;
    B[poct_txt]:::input -.->|if Optovue| E((oct-converter));

    E:::gear -.->|if output_npy is True| F[.npy NumPy array]
    E -.->|if output_image_type is<br />.png, .bmp, .tiff, .jpg, or .jpeg| G[Image montage as configured]
    E -.->|if output_video_type is<br />.avi or .mp4| H[Video as configured]
    E -.->|if output_dicom is True| I[One or more DICOM files]
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Upload file to acquisition container
1. Select file as input to gear
1. If Optovue, additionally input paired .txt file.
1. Gear converts input as configured and places output in acquisition container

### Use Cases

#### Use Case 1: OCT to DICOM

__*Conditions:*__

- File is a proprietary OCT filetype supported by oct-converter
- User is looking for a standardized filetype that works with other Flywheel gears in
their workflow

By configuring `output_dicom` to True, this gear will output one or more DICOMs.These
DICOMs can then be utilized in coordination with other Flywheel gears that support
DICOMs.

The amount of populated DICOM tags depends heavily on the filetype. Some filetypes
(.fda/.fds, .e2e) have a considerable amount of metadata contained in the file.
Others have minimal metadata.

#### Use Case 2: OCT to video

__*Conditions:*__

- File is an OCT filetype supported by oct-converter
- User is looking for a video representation of the image(s) contained for viewing

By configuring `output_video_type` to ".mp4" or ".avi", the gear will output the
configured video type. This can be utilized within a viewer that accepts the configured
video type to observe the sequential playback of the scan.

#### Use Case 3: OCT .img files

__*Conditions:*__

- File is a Zeiss .img file
- Gear defaults do not produce the expected output image/video/DICOM.

Zeiss .img files have additional manual configuration options to improve accuracy of
the output image. By default, .img files are parsed as rows = 1024 and columns = 512,
but it is possible to have a scan of a different size. The config options `img_rows`
and `img_columns` are for use in these instances. Additionally, if the gear defaults
produce a "stacked" image (two retinal images on one slice), `img_interlaced` is
provided to correct this output.

### Logging

This gear logs information about the gear run, including configuration, progress,
and (if applicable) errors encountered.

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]
<!-- markdownlint-disable-file -->
