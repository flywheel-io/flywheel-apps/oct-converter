# Release notes

## 2.0.0

Enhancements:

* Added .OCT (both Bioptigen and Optovue) support for .npy/video/image montage
conversion
* Added OCT to DICOM for supported filetypes
* Added config options `img_columns`, `img_rows`, and `img_interlaced` to allow for
manual control of Zeiss .img files for when the default config does not render the
image correctly
* Added config options `e2e_scalex`, `e2e_slice_thickness`, and
`e2e_extract_scan_repeats` to allow for manual control of Heidelberg .e2e files
* Updated gear to fit current best practices and CI/CD pipeline
* Expanded README
