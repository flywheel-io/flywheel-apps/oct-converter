"""Parser module to parse gear config.json."""

import logging
import os
import shutil
from pathlib import Path
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext

log = logging.getLogger(__name__)


# This function mainly parses gear_context's config.json file and returns
# relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[bool, dict]:
    """Parses context's config.json file to be used by gear

    Arguments
    ---------
    gear_context: to provide configuration information

    Returns
    -------
    bool: Debug configuration, default False
    dict: Arguments needed for the gear to run as configured, including
        * output_image_type: The type of image to output, ['.png', '.bmp', '.tiff', '.jpg', '.jpeg', 'none']
        * output_video_type: The type of video to output, ['.avi', '.mp4', 'none']
        * output_dicom: Whether to output DICOM(s)
        * montage_columns: Used with peek(), the number of columns in resulting montage image
        * montage_rows: Used with peek(), the number of rows in resulting montage image
        * img_rows: Manually configure rows. Used with img filetype.
        * img_rows: Manually configure columns. Used with img filetype.
        * img_interlaced: Manually set interlaced. Used with img filetype.
        * e2e_scalex: Manually configure x scale (in mm). Used with E2E filetype.
        * e2e_slice_thickness: Manually configure z scale (in mm). Used with E2E filetype.
        * e2e_extract_scan_repeats: Configure to keep all scans. Used with E2E filetype.
        * work_path: Pathlike to work directory
        * output_dir: Pathlike to output directory
        * filetype: Input filetype as parsed
    """

    log.info("Parsing inputs from gear context...")

    debug = gear_context.config.get("debug")
    config_dict = {
        "output_image_type": gear_context.config.get("output_image_type"),
        "output_video_type": gear_context.config.get("output_video_type"),
        "output_dicom": gear_context.config.get("output_dicom"),
        "output_npy": gear_context.config.get("output_npy"),
        "montage_columns": gear_context.config.get("montage_columns", 0),
        "montage_rows": gear_context.config.get("montage_rows", 0),
        "img_rows": gear_context.config.get("img_rows", 1024),
        "img_columns": gear_context.config.get("img_columns", 512),
        "img_interlaced": gear_context.config.get("img_interlaced"),
        "e2e_scalex": float(gear_context.config.get("e2e_scalex", 0.01)),
        "e2e_slice_thickness": float(
            gear_context.config.get("e2e_slice_thickness", 0.05)
        ),
        "e2e_extract_scan_repeats": gear_context.config.get(
            "e2e_extract_scan_repeats", False
        ),
        "output_dir": gear_context.output_dir,
    }

    if (
        config_dict["output_image_type"] == "none"
        and config_dict["output_video_type"] == "none"
        and not config_dict["output_dicom"]
        and not config_dict["output_npy"]
    ):
        log.error(
            "No outputs selected. At least one of image, video, DICOM, or .npy must be chosen as output."
        )
        os.sys.exit(1)

    # Move inputs into work_dir and identify filetype
    input_path = Path(gear_context.get_input_path("raw_input"))
    filename = input_path.name
    work_path = shutil.move(input_path, (gear_context.work_dir / filename))
    config_dict["work_path"] = work_path

    ext = filename.split(".")[-1].lower()
    if ext == "fda":
        config_dict["filetype"] = "topcon_fda"
    elif ext == "fds":
        config_dict["filetype"] = "topcon_fds"
    elif ext == "e2e":
        config_dict["filetype"] = "heidelberg_e2e"
    elif ext == "img":
        config_dict["filetype"] = "zeiss_img"
    elif ext == "oct":
        # Starts with bioptigen, if optovue txt file provided
        # the next chunk will overwrite.
        config_dict["filetype"] = "bioptigen_oct"
    else:
        # Raise filetype not supported.
        log.error(f"Input file extension {ext} not currently supported.")
        os.sys.exit(1)

    poct_txt_path = (
        Path(gear_context.get_input_path("poct_txt"))
        if gear_context.get_input_path("poct_txt")
        else None
    )
    if poct_txt_path:
        poct_filename = poct_txt_path.name
        shutil.move(poct_txt_path, gear_context.work_dir / poct_filename)
        config_dict["filetype"] = "optovue_oct"

    log.info(
        f"Input file extension {ext} identified as filetype {config_dict['filetype']}"
    )

    log.info("\tFinished parsing.")

    return debug, config_dict
