"""util.py module."""

import logging
from pathlib import Path

import numpy as np
from pydicom import dcmread
from pydicom.dataset import Dataset

log = logging.getLogger(__name__)


def bioptigen_rotate(ds: Dataset) -> Dataset:
    """Rotates pixel data 90 degrees + horizontal flip and adjusts relevant tags."""
    data = ds.pixel_array
    columns = ds.Columns
    rows = ds.Rows
    ds.Columns = rows
    ds.Rows = columns
    try:
        pixel_spacing = ds.PixelSpacing
        if isinstance(pixel_spacing, str):
            pixel_spacing = pixel_spacing.split(",")
        pixel_spacing.reverse()
        ds.PixelSpacing = pixel_spacing
    except AttributeError:
        pixel_spacing = (
            ds.SharedFunctionalGroupsSequence[0].PixelMeasuresSequence[0].PixelSpacing
        )
        if isinstance(pixel_spacing, str):
            pixel_spacing = pixel_spacing.split(",")
        pixel_spacing.reverse()
        ds.SharedFunctionalGroupsSequence[0].PixelMeasuresSequence[
            0
        ].PixelSpacing = pixel_spacing

    if len(data.shape) == 3:
        ds.PixelData = np.fliplr(np.rot90(data, k=1, axes=(1, 2))).tostring()
    else:  # 2D
        ds.PixelData = np.fliplr(np.rot90(data, k=1, axes=(0, 1))).tostring()

    return ds


def append_laterality_to_e2e_filename(file: Path):
    """Adds laterality to the DICOM filename, if laterality exists in DICOM."""
    dcm = dcmread(file)
    if dcm.Laterality:
        file.rename(Path(file.parent, f"{file.stem}_{dcm.Laterality}{file.suffix}"))
    else:
        log.warning(
            f"Laterality not found in DICOM file {file.name}, "
            "filename will not be changed to include laterality."
        )
