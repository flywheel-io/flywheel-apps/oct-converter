"""Main module."""

import logging

from .converter import OphthaConverter

log = logging.getLogger(__name__)


def run(config_dict):
    """
    Uses oct-converter package to read in data from a variety of sources and
    convert to publicly-accessible formats (e.g., .jpg, .mp4)

    Arguments
    ---------
    config_dict: Arguments needed for the gear to run as configured, including
        * output_image_type: The type of image to output, ['.png', '.bmp', '.tiff', '.jpg', '.jpeg', 'none']
        * output_video_type: The type of video to output, ['.avi', '.mp4', 'none']
        * output_dicom: Whether to output DICOM(s)
        * montage_columns: Used with peek(), the number of columns in resulting montage image
        * montage_rows: Used with peek(), the number of rows in resulting montage image
        * img_rows: Manually configure rows. Used with img filetype.
        * img_rows: Manually configure columns. Used with img filetype.
        * img_interlaced: Manually set interlaced. Used with img filetype.
        * e2e_scalex: Manually configure x scale (in mm). Used with E2E filetype.
        * e2e_slice_thickness: Manually configure z scale (in mm). Used with E2E filetype.
        * e2e_extract_scan_repeats: Configure to keep all scans. Used with E2E filetype.
        * work_path: Pathlike to work directory
        * output_dir: Pathlike to output directory
        * filetype: Input filetype as parsed

    Returns
    -------
    exit_code: int
        0 if no errors
    """
    # Call the template method for converting files
    ophtha_converter = OphthaConverter.factory(config_dict=config_dict)
    ophtha_converter.convert_file()
    return 0
