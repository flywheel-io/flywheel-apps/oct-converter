"""
Module for creating converter interfaces (abstract objects) and its instantiations, used
for converting files.
"""

import logging
import os
from abc import ABC, abstractmethod
from pathlib import Path

from oct_converter.dicom import create_dicom_from_oct
from oct_converter.image_types.oct import IMAGE_TYPES, VIDEO_TYPES
from oct_converter.readers import BOCT, E2E, FDA, FDS, IMG, POCT
from pydicom import dcmread

from .util import append_laterality_to_e2e_filename, bioptigen_rotate
from .utils.docs import Appender, create_shared_doc_vars

log = logging.getLogger(__name__)

_shared_docs, _shared_doc_kwargs = create_shared_doc_vars()
_shared_doc_kwargs.update(
    {
        "converter": """converter: oct_convter.readers
        An abstract class parameter implemented in the concrete objects, it's the object that
        is used for converting files. It is only the class and not an instance of it.""",
        "file_type": """file_type: str
        An abstract class parameter implemented in the concrete objects, it is the type of file
         that is being converted.""",
        "file_ext": """file_ext: str
        An abstract class parameter implemented in the concrete objects, it is the file
        extension that is read in the factory() method to select the concrete subclass
        of OphthaConverter.""",
        "path_file": """path_file: str
        The path the the file being read""",
        "output_dir": """output_dir: PathLike
        The output path of the output directory""",
        "oct_converter": """oct_converter: oct_converter.readers.object
        The instantiated oct-converter object, used for converting oct files.""",
        "oct_volumes": """oct_volumes: list
        Modified by a subclass' self.read_file() method, it contains the oct volumes
        extracted by an oct-converter object.""",
        "oct_fundus_images": """oct_fundus_images: list
        Modified by a subclass' self.read_file() method, it contains the fundus images
        extracted by an oct-converter object. Not all oct file types will contain fundus
        images.""",
        "config_dict": """config_dict: dict
        A dict containing the following keys:
        * output_image_type: The type of image to output, ['.png', '.bmp', '.tiff', '.jpg', '.jpeg', 'none']
        * output_video_type: The type of video to output, ['.avi', '.mp4', 'none']
        * output_dicom: Whether to output DICOM(s)
        * output_npy: Whether to output .npy(s)
        * montage_columns: Used with peek(), the number of columns in resulting montage image
        * montage_rows: Used with peek(), the number of rows in resulting montage image 
        * img_rows: Manually configure rows. Used with img filetype.
        * img_rows: Manually configure columns. Used with img filetype.
        * img_interlaced: Manually set interlaced. Used with img filetype.
        * e2e_scalex: Manually configure x scale (in mm). Used with E2E filetype.
        * e2e_slice_thickness: Manually configure z scale (in mm). Used with E2E filetype.
        * e2e_extract_scan_repeats: Configure to keep all scans. Used with E2E filetype.
        * work_path: Pathlike to work directory
        * output_dir: Pathlike to output directory
        * filetype: Input filetype as parsed""",
    }
)

# create descriptions for base and its concrete subclasses
_shared_doc_kwargs.update(
    {
        "OphthaConverter": """The abstract base class that acts as an interface for oct-converter objects.
    
This object is mainly used to read a raw input file with self.factory() method, which selects which subclass
to use for reading and saving OCT data, and then uses its template method self.convert_file()
to perform the conversion.""",
        "TopconFDAOphthaConverter": """An oct converter for Topcon .fda files.

It extracts an oct volume and a fundus image from a raw image file. It also gets metadata for both if applicable.""",
        "TopconFDSOphthaConverter": """An oct converter for Topcon .fds files.
    
It extracts an oct volume and a fundus image from a raw image file. It also gets metadata for both if applicable.""",
        "HeidelbergOphthaConverter": """An oct converter for Heidelberg .E2E files.
    
It extracts multiple oct volumes and fundus images found in a raw image file. It also gets metadata for both if applicable.""",
        "ZeissOphthaConverter": """An oct converter for Zeiss .img files.
    
It extracts an oct volume from a raw image file. It also gets metadata if applicable.""",
        "DicomOphthaConverter": """An oct converter for dicom (.dcm) files
    
It extracts an oct volume from a raw image file. It also gets metadata if applicable.""",
        "BioptigenOphthaConverter": """An oct converter for Bioptigen OCT (.OCT) files
    
It extracts an oct volume from a raw image file. It also gets metadata if applicable.""",
        "OptovueOphthaConverter": """An oct converter for Optovue (.OCT) files
    
It extracts an oct volume from a raw image file. It also gets metadata if applicable.""",
    }
)

_shared_docs["OphthaConverter"] = """
%(OphthaConverter)s

Params
------
%(converter)s
%(file_type)s
%(file_ext)s
%(path_file)s
%(output_dir)s
%(oct_converter)s
%(oct_volumes)s
%(oct_fundus_images)s
"""


@Appender(_shared_docs["OphthaConverter"] % _shared_doc_kwargs)
class OphthaConverter(ABC):
    @classmethod
    def factory(cls, config_dict):
        file_types = [sub.file_type for sub in cls.__subclasses__()]

        result = None

        for subclass in cls.__subclasses__():
            if subclass.file_type == config_dict["filetype"]:
                result = subclass(config_dict=config_dict)
                break

        if not result:
            raise ValueError(
                f"Could not locate {cls.__name__} with file type "
                f"'{config_dict['filetype']}'. Available file types: \n"
                f" {file_types}"
            )

        return result

    def __init__(self, config_dict):
        self.path_file = config_dict["work_path"]
        self.output_dir = config_dict["output_dir"]
        self.output_image_type = config_dict["output_image_type"]
        self.output_video_type = config_dict["output_video_type"]
        self.output_dicom = config_dict["output_dicom"]
        self.output_npy = config_dict["output_npy"]
        self.montage_columns = config_dict["montage_columns"]
        self.montage_rows = config_dict["montage_rows"]
        self.img_rows = config_dict["img_rows"]
        self.img_columns = config_dict["img_columns"]
        self.img_interlaced = config_dict["img_interlaced"]
        self.e2e_scalex = config_dict["e2e_scalex"]
        self.e2e_slice_thickness = config_dict["e2e_slice_thickness"]
        self.e2e_extract_scan_repeats = config_dict["e2e_extract_scan_repeats"]

        self.oct_converter = self.converter(
            self.path_file
        )  # instantiate the oct-converter object
        self.oct_volumes = []  # modified by self.read_file
        self.fundus_images = []  # modified by self.read_file
        self.dicoms = []
        self.output_files = []

    _shared_docs["convert_file"] = """
    A template method that runs the methods for converting raw
    ophthamology files.
    
    Params
    ------
    %(config_dict)s
    """

    @Appender(_shared_docs["convert_file"] % _shared_doc_kwargs)
    def convert_file(self):
        log.info("Starting conversion...")

        # Read the raw file
        log.info(
            "Reading raw file with oct-converter object %s ..."
            % type(self.oct_converter)
        )
        self.read_file()
        log.info("\tFinished reading oct volume.")

        # Save outputs
        log.info("Saving output types and headers (if applicable)...")
        self._save_output()
        if self.output_dicom:
            self.save_dicom()
        log.info("\tFinished.")

        pass

    @property
    @abstractmethod
    def converter(self):
        """
        Returns the object (i.e., one of the reader objects) used for converting
        the raw file.
        """
        pass

    @property
    @abstractmethod
    def file_type(self):
        """
        The file type of the raw input file to convert.
        """
        pass

    @property
    @abstractmethod
    def file_ext(self):
        """
        The extension of the raw input file to convert.
        """

    @abstractmethod
    def read_file(self) -> dict:
        """
        Updates self.oct_volumes and self.fundus_images.
        """

        pass

    def _save_output(self):  # noqa: PLR0912
        fpath, _ = os.path.splitext(self.path_file)
        _, fname = os.path.split(fpath)

        log.debug("num_rows: %s", self.montage_rows)
        log.debug("num_cols: %s", self.montage_columns)

        output_types = []
        if self.output_npy:
            output_types.append(".npy")
        if self.output_image_type in IMAGE_TYPES:
            output_types.append(self.output_image_type)
        if self.output_video_type in VIDEO_TYPES:
            output_types.append(self.output_video_type)

        for output_type in output_types:
            log.info("Saving output as type '%s'", output_type)

            log.info("\tSaving oct volumes if any were found")
            # save the oct_volumes if any were found
            for idx, oct_volume in enumerate(self.oct_volumes):
                # Append int if more than one volume was found
                if len(self.oct_volumes) == 1:
                    suffix = ""
                else:
                    suffix = "_" + str(idx)

                # Create output name
                filepath_out = os.path.join(
                    self.output_dir, fname + suffix + output_type
                )
                log.debug("\tfilepath_out: %s", filepath_out)

                try:
                    if output_type in IMAGE_TYPES:
                        if self.montage_columns > 0 and self.montage_rows > 0:
                            oct_volume.peek(
                                rows=self.montage_rows,
                                cols=self.montage_columns,
                                filepath=filepath_out,
                            )
                        elif self.montage_columns == 0 and self.montage_rows > 0:
                            oct_volume.peek(
                                rows=self.montage_rows, filepath=filepath_out
                            )
                        elif self.montage_columns > 0 and self.montage_rows == 0:
                            oct_volume.peek(
                                cols=self.montage_columns, filepath=filepath_out
                            )
                        else:
                            oct_volume.peek(filepath=filepath_out)
                    else:
                        oct_volume.save(filepath_out)
                    log.info("\tSaved '%s'", filepath_out)
                    self.output_files.append(Path(filepath_out))
                except Exception as exc:
                    log.error(
                        "Skipping file '%s' because got following error:\n %s"
                        % (filepath_out, exc)
                    )

            # save fundus images if any were found
            log.info("\tSaving fundus images if any were found...")
            for idx, fundus_image in enumerate(self.fundus_images):
                # Do not save fundus as video, only as image or numpy
                if output_type not in IMAGE_TYPES and output_type != ".npy":
                    continue
                # Append int if more than one volume was found
                if len(self.fundus_images) == 1:
                    suffix = "_fundus"
                else:
                    suffix = "_fundus_" + str(idx)

                filepath_out = os.path.join(
                    self.output_dir, fname + suffix + output_type
                )
                log.debug("\tfilepath_out: %s", filepath_out)

                try:
                    fundus_image.save(filepath_out)
                    log.info("\tSaved '%s'", filepath_out)
                    self.output_files.append(Path(filepath_out))
                except Exception as exc:
                    log.error(
                        "Skipping file '%s' because got following error:\n %s"
                        % (filepath_out, exc)
                    )

    @abstractmethod
    def save_output(self):
        pass

    def save_dicom(self):
        pass


@Appender(
    _shared_docs["OphthaConverter"]
    % {
        **_shared_doc_kwargs,
        **{"OphthaConverter": _shared_doc_kwargs["ZeissOphthaConverter"]},
    }
)
class ZeissOphthaConverter(OphthaConverter):
    converter = IMG
    file_type = "zeiss_img"
    file_ext = ".img"

    def read_file(self):
        """
        Updates oct_volumes with a list of all OCT volumes and additional metadata
        if available. It also updates self.fundus_images with a list of all fundus
        images and additional metadata if available.
        """

        self.oct_volumes = [
            self.oct_converter.read_oct_volume(
                rows=self.img_rows,
                cols=self.img_columns,
                interlaced=self.img_interlaced,
            )
        ]

    def save_output(self):
        pass

    def save_dicom(self):
        log.info("\tSaving as DICOM...")
        self.dicoms = create_dicom_from_oct(
            input_file=str(self.path_file),
            output_dir=str(self.output_dir),
            rows=self.img_rows,
            cols=self.img_columns,
            interlaced=self.img_interlaced,
        )


@Appender(
    _shared_docs["OphthaConverter"]
    % {
        **_shared_doc_kwargs,
        **{"OphthaConverter": _shared_doc_kwargs["HeidelbergOphthaConverter"]},
    }
)
class HeidelbergOphthaConverter(OphthaConverter):
    converter = E2E
    file_type = "heidelberg_e2e"
    file_ext = ".E2E"

    def read_file(self):
        """
        Updates oct_volumes with a list of all OCT volumes and additional metadata
        if available. It also updates self.fundus_images with a list of all fundus
        images and additional metadata if available.
        """

        self.oct_volumes = self.oct_converter.read_oct_volume(
            scalex=self.e2e_scalex, slice_thickness=self.e2e_slice_thickness
        )
        self.fundus_images = self.oct_converter.read_fundus_image(
            scalex=self.e2e_scalex, extract_scan_repeats=self.e2e_extract_scan_repeats
        )

    def save_output(self):
        pass

    def save_dicom(self):
        log.info("\tSaving as DICOM...")
        self.dicoms = create_dicom_from_oct(
            input_file=str(self.path_file),
            output_dir=str(self.output_dir),
            scalex=self.e2e_scalex,
            slice_thickness=self.e2e_slice_thickness,
            extract_scan_repeats=self.e2e_extract_scan_repeats,
        )

        for dcm in self.dicoms:
            append_laterality_to_e2e_filename(dcm)


@Appender(
    _shared_docs["OphthaConverter"]
    % {
        **_shared_doc_kwargs,
        **{"OphthaConverter": _shared_doc_kwargs["TopconFDAOphthaConverter"]},
    }
)
class TopconFDAOphthaConverter(OphthaConverter):
    converter = FDA
    file_type = "topcon_fda"
    file_ext = ".fda"

    def read_file(self):
        """
        Appends oct_volumes list with the single OCT volume it reads, and the
        volume can have metadata as well. It also appends self.fundus_images
        with the fundus image it reads, and this image can have metadata as well.
        """
        self.oct_volumes.append(self.oct_converter.read_oct_volume())
        self.fundus_images.append(self.oct_converter.read_fundus_image())

    def save_output(self):
        pass

    def save_dicom(self):
        log.info("\tSaving as DICOM...")
        self.dicoms = create_dicom_from_oct(
            input_file=str(self.path_file),
            output_dir=str(self.output_dir),
        )


# Inherit read_file method from TopconFDAOphthaConverter, since they do the same thing
@Appender(
    _shared_docs["OphthaConverter"]
    % {
        **_shared_doc_kwargs,
        **{"OphthaConverter": _shared_doc_kwargs["TopconFDSOphthaConverter"]},
    }
)
class TopconFDSOphthaConverter(TopconFDAOphthaConverter, OphthaConverter):
    converter = FDS
    file_type = "topcon_fds"
    file_ext = ".fds"

    def save_output(self):
        pass

    def save_dicom(self):
        log.info("\tSaving as DICOM...")
        self.dicoms = create_dicom_from_oct(
            input_file=str(self.path_file),
            output_dir=str(self.output_dir),
        )


@Appender(
    _shared_docs["OphthaConverter"]
    % {
        **_shared_doc_kwargs,
        **{"OphthaConverter": _shared_doc_kwargs["BioptigenOphthaConverter"]},
    }
)
class BioptigenOCTOphthaConverter(OphthaConverter):
    converter = BOCT
    file_type = "bioptigen_oct"
    file_ext = ".OCT"

    def read_file(self):
        """
        Appends oct_volumes list with the single OCT volume it reads, and the
        volume can have metadata as well. It also appends self.fundus_images
        with the fundus image it reads, and this image can have metadata as well.
        """
        octs = self.oct_converter.read_oct_volume()
        if octs:
            self.oct_volumes = octs
        fundus = self.oct_converter.read_fundus_image()
        if fundus:
            self.fundus_images = fundus

    def save_output(self):
        pass

    def save_dicom(self):
        log.info("\tSaving as DICOM...")
        self.dicoms = create_dicom_from_oct(
            input_file=str(self.path_file),
            output_dir=str(self.output_dir),
        )
        # Bioptigen seems to be rotated...
        for file in os.listdir(self.output_dir):
            if file.endswith(".dcm"):
                file_path = os.path.join(self.output_dir, file)
                ds = dcmread(file_path)
                ds = bioptigen_rotate(ds)
                ds.save_as(file_path)


@Appender(
    _shared_docs["OphthaConverter"]
    % {
        **_shared_doc_kwargs,
        **{"OphthaConverter": _shared_doc_kwargs["OptovueOphthaConverter"]},
    }
)
class OptovueOCTOphthaConverter(OphthaConverter):
    converter = POCT
    file_type = "optovue_oct"
    file_ext = ".OCT"

    def read_file(self):
        """
        Appends oct_volumes list with the single OCT volume it reads, and the
        volume can have metadata as well.
        """
        self.oct_volumes = self.oct_converter.read_oct_volume()

    def save_output(self):
        pass

    def save_dicom(self):
        log.info("\tSaving as DICOM...")
        self.dicoms = create_dicom_from_oct(
            input_file=str(self.path_file),
            output_dir=str(self.output_dir),
        )
